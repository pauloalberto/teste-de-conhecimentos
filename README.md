# Teste de Conhecimentos - Analista Desenvolvedor
------------------
Desenvolvido por: Paulo Alberto Polachini Boesing Ramos
Os exercícios 1,2,3 estão cada um dentro das pastas:
exercício-1
exercício-2
exercício-3


### Instalação

A API e o Front-end requerem o Apache com PHP versão 5.3 ou superior e banco de dados MySql 5.


```
Clone este repositório em sua pasta www ou htdocs
--------------------------------------------------------
CONFIGURANDO O FRONT-END:
Edite o arquivo front/js/tarefasController.js e modifique as seguintes variáveis de acordo com a pasta de onde os projetos serão executados no servidor:
_URL_API = 'http://localhost/teste-de-conhecimentos/api';
_URL_FRONT = 'http://localhost/teste-de-conhecimentos/front';
```

```
CONFIGURANDO O BACK-END:
Edite o arquivo api/config/app.php e altere a sessão Datasources nas chaves:
username (Coloque aqui o usuário do banco de dados)
password (Coloque aqui a senha do banco de dados)
database (Coloque aqui o nome da base de dados)
```

```
CRIE A SEGUINTE TABELA EM SEU BANCO DE DADOS
CREATE TABLE `tarefas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `ordem` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
)
```

### Rotas
```
tarefas/ - GET
Retorna todas as tarefas cadastradas.
```
```
tarefas/add - POST
{
    "titulo": string,
    "descricao": string
    "ordem": int
}
Cadastra uma nova tarefa
```
```
tarefas/edit - PUT
{
    "id" : int,
    "titulo": string,
    "descricao": string
    "ordem": int
}
Atualiza uma nova tarefa
```
```
tarefas/id/{id} - GET
retorna uma tarefa de acordo com o id fornecido
```
```
tarefas/delete - DELETE
{
    "id": int
}
Exclui uma tarefa de acordo com seu id
```

