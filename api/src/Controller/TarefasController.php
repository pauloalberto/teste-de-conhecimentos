<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefas Controller
 *
 * @property \App\Model\Table\TarefasTable $Tarefas
 *
 * @method \App\Model\Entity\Tarefa[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TarefasController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        
        $this->paginate['order'] = array('ordem' => 'asc');
        $tarefas = $this->paginate($this->Tarefas);

        $this->set([
            'my_response' => $tarefas,
            '_serialize' => 'my_response',
        ]);
        
        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * Get By Id
     *
     * @return \Cake\Http\Response|void
     */
    public function id()
    {
        $this->request->allowMethod(['get']);
        $id =  $this->request->query('id');
        
        $tarefa = $this->Tarefas->get($id);

        $this->set([
            'my_response' => $tarefa,
            '_serialize' => 'my_response',
        ]);
        
        $this->RequestHandler->renderAs($this, 'json');
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        
        $tarefa = $this->Tarefas->newEntity();
        if ($this->request->is('post')) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());
            if ($this->Tarefas->save($tarefa)) {
                $return = true;
            }else {
                $return = false;
            }
        }

        $this->set([
            'my_response' => $return,
            '_serialize' => 'my_response',
        ]);
        
        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        $this->request->allowMethod(['put']);

        $id =  $this->request->getData('id');

        $tarefa = $this->Tarefas->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->getData());
            if ($this->Tarefas->save($tarefa)) {
                $return = true;
            } else {
                $return = false;
            }
            
        }

        $this->set([
            'my_response' => $return,
            '_serialize' => 'my_response',
        ]);

        $this->RequestHandler->renderAs($this, 'json');    
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['delete']);

        $id =  $this->request->getData('id');
        $tarefa = $this->Tarefas->get($id);

        if ($this->Tarefas->delete($tarefa)) {
            $return = true;
        } else {
            $return = false;
        }


        $this->set([
            'my_response' => $return,
            '_serialize' => 'my_response',
        ]);

        $this->RequestHandler->renderAs($this, 'json');    
    }
}
