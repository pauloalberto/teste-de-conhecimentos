<?php 
    class Multiplos
    {
        private $startNumber    = 1;
        private $endNumber      = 100;
        private $result         = [];
        
        /**
         * Métodos gets
         */

        public function getStartNumber()
        {
            return $this->startNumber;
        }

        public function getEndNumber() 
        {
            return $this->endNumber;
        }

        /**
         * Métodos Sets
         */
        public function setStartNumber($number) 
        {
            $this->startNumber = $number;
        }

        public function setEndNumber($number)
        {
            $this->endNumber = $number;
        }

        /**
         * Métodos públicos
         */

         public function checkNumbers() {
             for($number = $this->startNumber; $number <= $this->endNumber; $number++)
             {
                if($this->isMultiple3($number) && $this->isMultiple5($number))
                {

                   array_push($this->result,'Fizz Buzz');

                }else if ($this->isMultiple3($number)) 
                {

                    array_push($this->result, 'Fizz');
                
                } else if ($this->isMultiple5($number))
                {

                    array_push($this->result, 'Buzz');

                } else {

                    array_push($this->result, $number);

                }
             }

             return $this->result;
         }

        /**
         * Métodos privados
         */

         // Método responsável por verificar se o número é múltiplo de 3
        private function isMultiple3($number)
        {
            if ($number % 3 == 0)
            {
                return true;
            } else {
                return false;
            }
        }

         // Método responsável por verificar se o número é múltiplo de 5
         private function isMultiple5($number)
         {
             if ($number % 5 == 0)
             {
                 return true;

             } else {

                 return false;
             }
         }
 
    }
?>