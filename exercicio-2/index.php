<?php
    require_once('utils.php');

    if ( !isLogged() ) {
        header("Location: http://www.google.com");
        exit();
    }