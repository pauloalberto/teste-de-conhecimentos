<?php
    /**
     * Função responsável por verificar se existe variáveis de sessão ou cookies do usuário autenticado.
     */
    function isLogged()
    {
        if (
            (
                isset($_SESSION['loggedin']) 
                && 
                isset($_COOKIE['Loggedin'])  
            )
            &&
            (
                $_SESSION['loggedin']   == true 
                &&
                $_COOKIE['Loggedin']    == true
            )     
        )
        {
            return true;

        }else {
            
            return false;
        }
    }