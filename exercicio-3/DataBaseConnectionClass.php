<?php
    class DataBaseConnectionClass
    {
        private $host     = '127.0.0.1';
        private $user     = 'root';
        private $password = 'mysql';
        private $db       = 'teste';
        private $dbConn   = null;

        /**
         * Métodos gets
         */
        
         public function getHost()
         {
             return $this->host;
         }

         public function getUser()
         {
             return $this->user;
         }

         public function getPassword()
         {
             return $this->password;
         }

         public function getDbName()
         {
             return $this->dbName;
         }

         /**
          * Métodos sets
          */

          public function setHost($host)
          {
              $this->host = $host;
          }

          public function setUser($user)
          {
              $this->user = $user;
          }

          public function setPassword($password)
          {
              $this->password = $password;
          }

          public function setDbName($dbName)
          {
              $this->dbName = $dbName;
          }

          /**
           * Métodos públicos
           */

           public function Connect()
           {
                try 
                {
                   return $this->dbConn = new PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->password);

                } catch(PDOException $e) 
                {
                    echo $e->getMessage();
                    exit();
                }
           }
    }