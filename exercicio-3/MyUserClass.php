<?php
    class MyUserClass
    {
        private $connection;

        public function __construct($connection = null)
        {
            if($connection instanceof PDO)
            {
                $this->connection = $connection;

            }else 
            {
                echo "Você deve fornecer uma instância do banco de dados para usar essa classe";
                exit();
            }
        }
        
        public function getUserList()
        {
            $results = $this->connection->query('select name from user order by name');
            return $results;
        }
    }