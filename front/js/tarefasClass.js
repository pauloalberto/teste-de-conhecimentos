var Tarefas = function () {
    var PRIVATE = {};
    var PUBLIC = {};
    var PROPERTIES = {};

    /***************************************************
     * Métodos privados
     ***************************************************/

        /**
         * Método responsável por requisições ajax
         * Aceita um Json com as seguintes propriedades:
         * method: Tipo de requisição a ser executada pelo servidor, default: POST
         * async: Aceita requisições assíncronas sem travar a tela do usuário, default: true
         * data: Dados a serem enviados para o servidor
         * url: Url da requisição
         * headers: Json com os headers que serão enviados para o servidor
         * headers.DataType: tipo de dado que será enviado para o servidor, default: json
         * headers.ContentType: tipo de conteúdo que será enviado para o servidor, default: application/json
         * headers.Accept: tipo de resposta esperada de retorno do servidor, default: text/html;charset=UTF-8
         */
        PRIVATE.ajax = function ajax(configs) {

            configs = configs || { }
            configs.method = configs.method || 'POST';
            configs.async  = configs.async  || true;
            configs.data   = configs.data || {};
            configs.url    = configs.url || '';

            configs.headers = configs.headers || {};
            configs.headers.DataType = configs.headers.DataType || 'json';
            configs.headers.ContentType = configs.headers.ContentType || 'application/json';
            configs.headers.Accept = configs.headers.Accept || 'text/html;charset=UTF-8';
        
        return $.ajax({
            url: configs.url,
            method: configs.method,	
            async: configs.async,
            data: configs.data,

            headers:{
                "DataType": configs.headers.DataType,
                "ContentType": configs.headers.ContentType,
                "Accept": configs.headers.Accept
                }
            });
        }

    /***************************************************
     * Métodos públicos
     ***************************************************/

        /**
         * Método que mostra o loading na tela
         * Aceita um JSON com as seguintes propriedades
         * width: tamanho em px da imagem de load, default: 50px
         * show: se é para exibir ou ocultar o loading, default: false
         * rootElm: qual container html irá receber o loading, default: null
         * clearContent: se é para limpar todo o container html para aplicar o loading, default: true
         */
        PUBLIC.loading = function loading(configs){
            configs               = configs || {}
            configs.width         = configs.width || '25px';
            configs.show          = configs.show || false;
            configs.rootElm       = configs.rootElm || null;
            configs.clearContent  = configs.clearContent == undefined ? true : configs.clearContent;
            configs.gif           = configs.gif || 'ajax-loader.gif';
            configs.hiddenContent = configs.hiddenContent || false;
            var html = '<div style=\"text-align:center\"> \
                            <img src="imagens/'+ configs.gif +'" class="loading" style="width: ' + configs.width + '">\
                        </div>';

            if(configs.clearContent)
                $(configs.rootElm).html('');

            if(configs.hiddenContent){
                $(configs.rootElm).children().css('display','none');
            }

            if(configs.show)
                $(configs.rootElm).append(html);
            else{
                $(configs.rootElm + ' .loading').remove();
                $(configs.rootElm).children().css('display','block');
            }
                
        }

        /**
         * Método que busca as tarefas cadastradas
         * url: url da requisição
         */
        PUBLIC.getTasks = function getTasks(body) {
            body = body || {};
            body.url = body.url || '';

            return PRIVATE.ajax({
                url: body.url,
                method: 'GET'
            });
        }

        /**
         * Método que busca uma tarefa por seu id
         * url: url da requisição
         */
        PUBLIC.getTaskById = function getTaskById(body) {
            body = body || {};
            body.url = body.url || '';
            body.data = body.data || null;

            return PRIVATE.ajax({
                url: body.url,
                data: body.data,
                method: 'GET'
            });
        }

        /**
         * Método responsável por excluir sessões do usuário
         * sessionID: id da sessão que será deletada, default: null
         */
        PUBLIC.deleteTask = function deleteTask(body) {
            body = body || {};
            body.data = body.data || null;

            return PRIVATE.ajax({
                url: body.url,
                data: body.data,
                async: false,
                method: 'DELETE'
            });
        }

        /**
         * Método que cadastra a tarefa no banco de dados
         */
        PUBLIC.createTask = function createTask(body) {
            body = body || {};
            body.url = body.url || null;
            body.data = body.data || null;

            return PRIVATE.ajax({
                url: body.url,
                data: body.data,
                async: false,
                method: 'POST'
            })
        }


        /**
         * Método que atualiza a tarefa no banco de dados
         */
        PUBLIC.updateTask = function updateTask(body) {
            body = body || {};
            body.url = body.url || null;
            body.data = body.data || null;

            return PRIVATE.ajax({
                url: body.url,
                data: body.data,
                async: false,
                method: 'PUT'
            })
        }




    return PUBLIC;
}