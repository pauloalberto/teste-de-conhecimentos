_URL_API = 'http://localhost/teste-de-conhecimentos/api';
_URL_FRONT = 'http://localhost/teste-de-conhecimentos/front';

$(document).ready(function(){

    var tarefas = new Tarefas();

    tarefas.loading({show: true,rootElm: '.tarefas'});

    /**
     * Busca as tarefas ao inicializar o front
     */
    tarefas.getTasks(
    {
        url: _URL_API+'/tarefas'
    }).then(function(lista){
        tarefas.loading({show: false,rootElm: '.tarefas'});
        var html = '';
        if(lista.length > 0) 
        {
            for(x = 0 ; x < lista.length; x++)
            {
                html += '<li class="tarefa'+lista[x].id+'" data-id="'+lista[x].id+'">' + lista[x].titulo + '\
                            <img src="imagens\\excluir.png" class="excluir" data-id="'+lista[x].id+'">\
                            <img src="imagens\\editar.png" class="editarRegistro" data-id="'+lista[x].id+'">\
                            <div>' + lista[x].descricao + '</div>\
                        </li>';
            }

            $('.tarefas').append(html);
        }
    }, function () 
    {
        alert( "Ocorreu um erro ao buscar as tarefas.");
        tarefas.loading({show: false,rootElm: '.tarefas'});
    });

    /**
     * Habilita o método de arrastar e soltar
     */
    $("ol.tarefas").sortable({

        // Quando terminar de arrastar salva as novas posições no banco de dados
        onDrop: function($item, container, _super) {
                $item.find('ol.tarefas').sortable('enable');
                _super($item, container);

                $("ol.tarefas li").each(function(i,v){
                    var id = $(this).data('id');
                    tarefas.updateTask({
                    data: {
                        id : id,
                        ordem: i
                    },
                    url: _URL_API + '/tarefas/edit'
                });                    
            });
        }
    });

    /**
     * Ação executada ao clicar em excluir uma tarefa
     */
    $(document).on('click','.excluir', function(){
        var id = $(this).data('id');

        if(confirm('Deseja realmente excluir esta tarefa?'))
        {
            tarefas.deleteTask({
                data: {id: id},
                url: _URL_API + '/tarefas/delete'
            }).then(function(data) 
            {
                if(data == true)
                {
                    $('.tarefa' + id ).remove();
                }else 
                {
                    alert('Erro ao excluir a tarefa');
                }
                
            }, function() {
                alert( "Ocorreu um erro ao excluir a tarefa.");
                tarefas.loading({show: false,rootElm: '.tarefas'});
            });
        }
    });

    /**
     * Ação executada ao clicar em um ítem do menu
     */
    $(document).on('click','.menu span', function() {
        menu($(this).data('item'));
    });

    /**
     * Ação a ser executada ao clicar em cadastrar uma nova tarefa
     */
    $(document).on('submit','.frm_cadastrar', function(e) {
        e.preventDefault();
        tarefas.createTask({
            data: $('.frm_cadastrar').serialize(),
            url: _URL_API + '/tarefas/add'
        }).then(function(data) 
        {
            if(data == true)
            {
                alert('Tarefa cadastrada com sucesso');
                document.location.href = _URL_FRONT;
            }else 
            {
                alert('Erro ao cadastrar a tarefa');
            }
            
        }, function() {
            alert( "Ocorreu um erro ao cadastrar a tarefa.");
            tarefas.loading({show: false,rootElm: '.tarefas'});
        });            
    });

    /**
     * Ação executada ao clicar em editar uma tarefa
     */
    $(document).on('click','.editarRegistro', function(){
        var id = $(this).data('id');

        tarefas.getTaskById({
            data: {id: id},
            url: _URL_API + '/tarefas/id'
        }).then(function(data) 
        {
            $('.frm_editar input[name="titulo"]').val(data.titulo);
            $('.frm_editar textarea[name="descricao"]').val(data.descricao);
            $('.frm_editar input[name="ordem"]').val(data.ordem);
            $('.frm_editar input[name="id"]').val(data.id);

            menu('editar');
        }, function() {
            alert( "Ocorreu um erro ao buscar os dados da tarefa.");
            tarefas.loading({show: false,rootElm: '.tarefas'});
        });

    });    
    
    /**
     * Ação executada em atualizar uma tarefa
     */
    $(document).on('submit','.frm_editar', function(e) {
        e.preventDefault();
        tarefas.updateTask({
            data: $('.frm_editar').serialize(),
            url: _URL_API + '/tarefas/edit'
        }).then(function(data) 
        {
            if(data == true)
            {
                alert('Tarefa editada com sucesso');
                document.location.href = _URL_FRONT;
            }else 
            {
                alert('Erro ao editar a tarefa');
            }
            
        }, function() {
            alert( "Ocorreu um erro ao atualizar a tarefa.");
            tarefas.loading({show: false,rootElm: '.tarefas'});
        });            
    });        
});